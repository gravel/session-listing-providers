# Session Listing Provider protocol

## Live demo

A demo of the listing provider protocol is available at <https://lp.sessioncommunities.online>.
Does not include onion request endpoint.

## Mission statement

A listing provider should inform the user of existing Community Servers, organized in one or more listings and supplemented with information provided by the maintainer.

A listing provider should also include an endpoint to retrieve all listings currently available.

A listing provider should be available via Session's onion request to provide anonymity to users while requesting listings.

## Approaches to extending the SOGS API

Given that the listing provider API development is independent of the SOGS API and conflicts between data keys can arise, there are several approaches to consider. To ensure consistency, the approach to **wrap all potentially conflicting data keys** should be considered binding to all endpoints if they combine data from independent sources (independent in specification).

```json5
// Note: Key names subject to change.

// Wrap both:

{
    "room": {
        "name": "Community Name",
        "description": "Lorem ipsum dolor sit amet"
    },
    "room_extra": {
        "join_url": "...",
        "language_flag": "🇬🇧",
        "tags": ["maintainer-tag"]
    }
}

```

## Maintainer-provided info

In addition to information obtained from the SOGS API, the listing provider must include the following information:

- Join URL of Community and/or information needed to reconstruct it.
  - Reason: User must be able to connect to the Community server securely.
- For each listing:
  - Listing identifier.
  - Human-readable name.

The listing provider may include the following information:

- For each listing:
  - Date of creation.
  - Date of last update.
- For Communities or Community listings:
  - Age rating and/or observed degree of compliance with Session Terms of Service.
  - Date maintainer has last verified age rating and/or observed ToS compliance.
  - Date of inclusion in listing provider.
- For Communities:
  - Language(s) spoken in the Community.
  - Metric(s) of recent message frequency.
  - Additional description.
  - User-provided and/or maintainer-provided tags.
  - SOGS-provided server-wide name and icon.

## Draft of endpoint specifications

### POST `/oxen/v4/lsrpc`

Description: Parse a Session onion request and proceed with the corresponding endpoint.

Error responses:

- TBD

See <https://github.com/oxen-io/session-pysogs/blob/dev/sogs/routes/onion_request.py#L269>.

### GET `/`

Description: Show a human-readable HTML page describing the service.

### GET `/listings`

Description: Enumerate the listings hosted by the listing provider with listing-specific details.

Example response:

```json
{
    listings: [
      {
          id: "tech",
          name: "Tech Communities",
          rooms: 5
      },
      {
          id: "privsec",
          name: "Privacy & Security",
          rooms: 4
      },
      {
          id: "casual",
          name: "Casual Communities",
          rooms: 3
      },
      {
          id: "nsfw",
          name: "18+ Communities",
          rooms: 3
      }
      {
          id: "all"
          name: "All Communities",
          rooms: 10
      }
    ]
}
```

### GET `/listing/<listingId>`

Description: Show information about the listing with the given ID.

Error responses:

- `404 Not Found`
  - Returned if the given listing does not exist.
