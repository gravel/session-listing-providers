# About

## Disclaimer

I'm [`gravel`](https://codeberg.org/gravel/gravel), a Session user who has made extensive contributions to [sessioncommunities.online](https://sessioncommunities.online), an online listing of Session Communities. This proposal presents a way for me to keep giving back to the Session userbase. Whether you see this as a sign of bias or experience, I believe the points laid out below make a strong case for the implementation of Listing Providers in Session Messenger.

## Session Communities

[Session Communities](https://getsession.org/faq#open-groups) are chat rooms available in the [Session Messenger](https://getsession.org/). Communities are hosted by individuals on instances of the [Session Open Group Server (SOGS)](https://github.com/oxen-io/session-pysogs).

As with all Session chats, they communicate with apps solely through onion routing. Session Communities also include their own file hosting, contrary to direct messages and closed groups, where [centralized servers provided by the Session team](https://github.com/oxen-io/session-file-server) are responsible for storing the encrypted attachments. Contrary to DMs and groups and owing to their unlimited user capacity, the content in Communities is not end-to-end encrypted, which should come as no surprise. Session Communities represent part of the same trend as [Discord servers](https://discord.com/) or [Matrix chatrooms](https://matrix.org/).

The user can join a small selection of official Communities from the Session apps. If the user wishes to join any third-party Communities, they have to copy-paste a join URL into a textbox and press the "Join" button. At time of writing, there are around 120 known Communities.

## Accessibility and discoverability issues

There are a number of difficulties users face before discovering the wide palette of Communities maintained by Session users.

- The app provides little guidance on how to participate in Communities.
  - The only in-app Communities are official read-only Communities from the Oxen team.
  - These only contain updates on Oxen projects.
  - Their official Community for discussing Session was taken off the default list due to moderation issues.
- No intuitive way of joining a Community is available.
  - Clicking on a Community join link brings the user to a 'Not Found' page in the browser instead of being captured by the app.
  - Posting a Community join link in the app provides no invite embed. Invite embeds can only be sent to existing contacts via an unassuming "Invite" feature.
  - A separate web preview at `/r/<room>` does exist for each Community ([example](https://open.getsession.org/r/session)). The Session Open Group Server also has an index page with Communities hosted therein. However, these previews are bare-bones, not very self-explanatory, nor reachable from the app.

## Current discovery pipeline

The current process for a new user of Session to successfully discover and join a user-maintained Community is as follows:

1. Stumble onto the Join Community dialog.
1. Search “Session Communities” online.
1. Find the official Session Community or skip to step 7.
1. (*Optional*) Engage in off-topic conversation in the official Session Community.
1. Deduce or be told the existence of third-party Communities. Ask where to find more Communities.
1. (*Optional*) Attempt to open a Community join link and be faced with a "404 Not Found" response.
1. Visit online listings of Session Communities ([example 1](https://sessioncommunities.online), [example 2](https://session.directory))
1. (*Optional*) Visit the browser preview of a Community ([example without chat preview](http://sog.caliban.org/r/privacy)).
1. Copy the Join URL from the online listing.
1. (*Optional*) Attempt to open the Community join link in the browser and be faced with a "404 Not Found" response.
1. Navigate back to the Join Community dialog and paste the URL there.
1. (*Optional*) Accidentally erase a character from the URL and receive an "Invalid URL" error.
1. Click "Join".
1. (*Optional*) Go back to the main menu only to find the Community hasn't yet shown in the list of open chats.

As you can see, there is room for improvement in the way users discover Session Communities. While developers may have hidden hints of a Community discoverability feature in their roadmap, the truth is that any such feature has very low priority, as do Communities in general. As such, it is likely that we will not see any such feature in the near future.

As Session grows, a feature is needed in the interim to keep the pressure on Communities, keep them evolving and show their true potential. Otherwise, a future discovery method will leave the Communities wholly unprepared to handle waves of new users.

## Honorable mentions

Below follow some alternate solutions to provide Community discoverability that, in my opinion, are less likely to work:

- Including a link to an existing online listing of Session Communities.
  - Solutions relying on third-party resources have been ruled out by the developers.
  - While easy, this only skips a few of the steps in the pipeline.
- Session team manually reviews and adds Communities to the app.
  - Smaller Communities miss out on this privilege.
  - This adds a lot of overhead on the team, which is currently under-staffed. The team has the responsibility to censor and can be pressured to do so, leading to hostility between users and the team.
  - A moderation lapse or untoward atmosphere in any of the listed Communities may take a while for the team to detect and could eventually mean a scrap for this entire feature.
- Community servers are allowed to purchase [Oxen Name System entries](https://oxen.io/oxen-name-system-ons-the-facts) and be listed in the app based on this purchase.
  - This description greatly under-estimates the work needed to implement this feature, including a new ONS namespace and a service node backend for fetching Communities. The developers don't place enough importance on Communities to spend time on this.
  - This solution dodges the censorship question. In reality, not all Communities with an ONS entry can be allowed in official clients, so we are back at square one.
- Clients diverge in listed Communities based on their censorship liability.
  - Taking inspiration from Telegram, this means the iOS and Google Play Store versions of Session would list a limited number of official Communities, known to be well-moderated, while the Desktop and [F-Droid](https://f-droid.org/) version take more liberty with the Communities displayed.
  - However, the project's speed already greatly suffers from the need for feature parity accross platforms. This would further complicate efforts to unify parts of the codebases.
  - While censorship becomes less of a concern with this approach, the utility falls greatly, as fewer mainstream users (whom Session markets to) would have access to this feature. Powerusers downloading Session from F-Droid are more likely to be able to navigate through a clumsy user experience.
  - Assuming the apply & review model for listing Communities, the time the Session team would put into responding to new applicants or reports of abuse would fall as a result of this lower utility, resulting in poor coverage or a bad reputation.
- Adding a default read-only Community to inform new users of the existence of third-party Communities. (Proposal by me.)
  - While meaningful and unproblematic to implement, this change only tackles the information void surrounding Communities when it comes to new users, not the unintuitive user experience.
  - CTO Kee Jeffreys has expressed doubt that instructing users to search for online Community listings would bring much utility. At the same time, the team is not willing to share a link to any particular Community listing in official channels.
  - Note: This approach takes as precedent the official [`session-farsi`](https://open.getsession.org/r/session-farsi/) Community. This read-only Community was established and shown to all new users following crackdowns on protests by the fundamentalist Iranian government, giving the new wave of users instructions on joining [Ian MacDonald's Persian-language Community](https://sog.caliban.org/r/persian).

## Listing provider model: The Why

- Communities are in the hands of experienced Session users. It therefore stands to reason that experienced Session users should maintain lists of Communities, engage with rule-breakers and choose the correct Communities to pass on into the app.
- User choice is important. By default, only a few well-moderated Communities may be shown, but a user may want to see a full list of Communities. For this reason, there can be no singular first-party source of truth on the Communities available. The maintainer of a listing provider may choose to provide multiple listings of Communities, grouped by their safety. As active users of Session, they can readily make that judgement.
- The Session team can set up a much more direct line of communication with the maintainers of those listing providers recommended in the app. The terms of inclusion in the app can be laid out with solely these parties, and the listing providers maintainers can gain more insight into the preferences of the team through repeat interaction.
- Listing providers are the natural progression for the in-app integration of the user-made Community listings that currently fill the void in Community discoverability.

## Listing provider model: The What

One can imagine a [simple REST API](PROTOCOL.md) ([live demo](https://lp.sessioncommunities.online/)) to retrieve a list of communities from a listing provider; all Session would have to do is include the following UI below the community list:

```txt
Add more communities from list:
[ Listing provider URL | Add ]
```

The resulting communities can be plainly appended to the existing list, or split by provider. Furthermore, the community list can respond to new additions by way of daily polling and/or an additional button to re-fetch the community lists.

Understandably, this UX isn't beginner-friendly, and still requires guidance from community members. Crucially, however, when given a provider link, users are no longer stuck trying to open community URLs in the browser, as we can learn from our mistakes and design them in such a way that they natively open in the app.

### Default providers

One further step Session can take towards discovery is including default listing providers. The UI may be as follows:

```txt
Community lists:

( (S) Official Communities )
( 🇮🇷 Farsi Session Guide | x )

Add communities:

( 🌐 Languages (caliban.org) | + )
( 🖥️ Tech (caliban.org) | + )
( 👤 Zcyph's groups (...) | + )
etc.

( Add from URL ... )
```

The inclusion of default listing providers (and providers included, but disabled by default) is superior to the inclusion of default Communities, as curation is delegated to community members managing said listings. One group turned sour in an unofficial listing does not raise corporate eyebrows in quite the same way; no longer would Session need to micro-manage groups based on reputation.

Community members tasked with managing default listing providers are likely to be aware of the need to keep an eye on their listed groups, as well as the security needed to prevent a compromise. Channels to communicate with default listing curators with de-listing periods in case of non-cooperation would ease the resolution of any issues.
